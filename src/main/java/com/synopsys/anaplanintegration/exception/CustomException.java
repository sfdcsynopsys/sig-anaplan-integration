package com.synopsys.anaplanintegration.exception;

public class CustomException extends RuntimeException{
	
	private static final long serialVersionUID = 8305484321453477206L;

	public CustomException(String message) {
		super(message);
	}

}
