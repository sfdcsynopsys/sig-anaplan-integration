package com.synopsys.anaplanintegration.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synopsys.anaplanintegration.entity.SfdcProperties;
import com.synopsys.anaplanintegration.exception.CustomException;
import com.synopsys.anaplanintegration.service.AnaplanOperationsService;
import com.synopsys.anaplanintegration.service.SfdctoAnaplanService;

@Component
public class SfdcScheduler {

	@Value("${plan.dailyJob}")
	private String dailyJobexportId;

	@Value("${plan.adhocJob}")
	private String adhocJobexportId;

	@Value("${plan.date.fileId}")
	private String fileId;

	@Value("${plan.date.processId}")
	private String processId;

	@Value("${plan.master.processId}")
	private String dateProcessId;

	@Value("${plan.adhocJob.clearRequest}")
	private String clearProcessId;

	@Autowired
	SfdctoAnaplanService sfdctoAnaplanService;

	@Autowired
	AnaplanOperationsService anaplanOperationsService;

	ObjectMapper mapper = new ObjectMapper();

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	private static final Logger logger = LoggerFactory.getLogger(SfdcScheduler.class);

	@Scheduled(cron = "0 30 5 * * ?")
	public void dailyJob() {

		String result;

		try {

			logger.info("Daily Job Scheduler Started");

			result = anaplanOperationsService.runExportAction(dailyJobexportId);

			logger.info(result);

			List<SfdcProperties> sfdcProperties = mapper.readValue(result, new TypeReference<List<SfdcProperties>>() {
			});

			if (!sfdcProperties.isEmpty()) {

				sfdctoAnaplanService.exportFromSfdctoAnaplan(sfdcProperties);

				logger.info("Daily Job Scheduler Completed");
			} else {
				logger.info("No Request Has Found");
			}

		} catch (Exception ex) {
			throw new CustomException(ex.getMessage());
		}

	}

	@Scheduled(cron = "0 */5 * ? * *")
	public void adhocJob() {

		String result;

		try {

			logger.info("Adhoc Job Scheduler Started");

			result = anaplanOperationsService.runExportAction(adhocJobexportId);

			logger.info(result);

			List<SfdcProperties> sfdcProperties = mapper.readValue(result, new TypeReference<List<SfdcProperties>>() {
			});

			if (!sfdcProperties.isEmpty()) {

				sfdctoAnaplanService.exportFromSfdctoAnaplan(sfdcProperties);

				anaplanOperationsService.runProcess(clearProcessId);
				logger.info("Adhoc Job Scheduler Completed");
			} else {
				logger.info("No Adhoc Request Has Submitted");
			}

		} catch (Exception ex) {
			throw new CustomException(ex.getMessage());
		}

	}

	@Scheduled(cron = "0 0 6 * * ?")
	public void uploadDate() {
		String filestream = "Current Date" + "\n" + dateFormat.format(new Date());

		anaplanOperationsService.uploadFile(fileId, filestream);

		anaplanOperationsService.runProcess(processId);

		logger.info("Current Date is Uploaded in to Anaplan Data Hub");

		anaplanOperationsService.runMasterProcess(dateProcessId);

		logger.info("Current Date is Uploaded in to Master Model");

	}

}
