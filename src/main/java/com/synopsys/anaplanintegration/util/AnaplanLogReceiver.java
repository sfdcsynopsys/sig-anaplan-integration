package com.synopsys.anaplanintegration.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.synopsys.anaplanintegration.entity.SfdcSyncResponse;
import com.synopsys.anaplanintegration.service.AnaplanOperationsService;

@Component
public class AnaplanLogReceiver {

	@Value("${plan.log.fileId}")
	private String logFileId;

	@Value("${plan.log.processId}")
	private String logProcessId;

	@Autowired
	AnaplanOperationsService anaplanOperationsService;

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private static final SimpleDateFormat LogId = new SimpleDateFormat("ddMMyyyyHHmmssSSS");

	private static final Logger logger = LogManager.getLogger(AnaplanLogReceiver.class);

	@JmsListener(destination = "AnaplanLogQueue", containerFactory = "myFactory")
	public void receiveMessage(SfdcSyncResponse sfdcSyncResponse) {

		StringBuilder logData = new StringBuilder();
		sfdcSyncResponse.setTimeStamp(dateFormat.format(new Date()));
		sfdcSyncResponse.setLogId(LogId.format(new Date()));

		logData.append("LogId	Time	Trigger	Object	Query	Result	Count	Message");

		logData.append("\n");

		logData.append(sfdcSyncResponse.getLogId() + "	" + sfdcSyncResponse.getTimeStamp() + "	"
				+ sfdcSyncResponse.getTrigger() + "	" + sfdcSyncResponse.getFileName() + "	"
				+ sfdcSyncResponse.getQuery() + "	" + sfdcSyncResponse.getResult() + "	"
				+ sfdcSyncResponse.getCount() + "	" + sfdcSyncResponse.getMessage());

		logger.info(logProcessId);
		anaplanOperationsService.uploadFile(logFileId, logData.toString());
		anaplanOperationsService.runProcess(logProcessId);	

	}

}
