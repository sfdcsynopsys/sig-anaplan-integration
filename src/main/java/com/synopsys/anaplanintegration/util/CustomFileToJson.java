package com.synopsys.anaplanintegration.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class CustomFileToJson {

	private static final Logger logger = LogManager.getLogger(CustomFileToJson.class);
	
	public String convertToJson(String fileData)
	{
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		
		String arrayToJson = "";
		
		List<HashMap<String, String>> jsonData = new ArrayList<HashMap<String, String>>();
		
		String[] splitRows = fileData.split("\\r?\\n");
		
		String[] headers=splitRows[0].split("	",-1);
		
		for(int i=1;i<splitRows.length;i++)
		{
			HashMap<String, String> objectmap = new HashMap<>();
			String[] splitColoumn = splitRows[i].split("	",-1);
			if(splitColoumn.length>0)
			{
				for(int j=0;j<headers.length;j++)
				{
				     objectmap.put(headers[j], splitColoumn[j]);	
				}
			}
			
			if(objectmap!=null && !objectmap.isEmpty() && objectmap.size()>0)
			
				jsonData.add(objectmap);
			
		}		
		
		try {
			arrayToJson = objectMapper.writeValueAsString(jsonData);
		} catch (JsonProcessingException jsonException) {
			jsonException.printStackTrace();
		}
		
		return arrayToJson;
		
	}
	
/*	
	public List<SfdcProperties> convertToOject(String fileData)
	{
		
		List<SfdcProperties> sfdcProperties=new ArrayList<SfdcProperties>();
		
        String[] splitRows = fileData.split("\\r?\\n");
		
		for(int i=1;i<splitRows.length;i++)
		{
		
			SfdcProperties propertiesObj=new SfdcProperties(splitRows[0].split(",",-1));
			sfdcProperties.add(propertiesObj);	
		}	
		
		
		return sfdcProperties;
	}
	*/
	
	
	
}
