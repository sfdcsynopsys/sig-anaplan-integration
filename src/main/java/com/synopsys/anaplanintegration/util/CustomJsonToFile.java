package com.synopsys.anaplanintegration.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.synopsys.anaplanintegration.entity.Opportunity;
import com.synopsys.anaplanintegration.entity.SalesQualifyingLead;

public class CustomJsonToFile {

	private static final Logger logger = LogManager.getLogger(CustomFileToJson.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public String convertSalesToFile(List<SalesQualifyingLead> salesResponse) {

		StringBuilder content = new StringBuilder();

		content.append("\n");

		salesResponse.stream().forEach(sales -> {

			String salesAccount,sdrSourced=null;

			if (sales.getSalesAccount() == null)
				salesAccount = null;
			else
				salesAccount = sales.getSalesAccount().getName();
			
			if(sales.getSdrSourced()!= null)
				sdrSourced=sales.getSdrSourced().getName();

			content.append(sales.getOpportunityId() + "	" + sales.getPlaneSourced() + "	"
					+ sales.getSalesQualifiedTerritory() + "	" + sales.getRecordType().getName() + "	"
					+ sdrSourced + "	" + sales.getSalesQualifiedDate() + "	" + sales.getSalesStatus()
					+ "	" + sales.getBusiness_Type_c() + "	" + salesAccount + "	" + sales.getName() + "	"
					+ sales.getStageName() + "	" + sales.getAmount() + "	" + dateFormat.format(new Date()));
			content.append("\n");
		});

		return content.toString();
	}

	public String convertOpportunityToFile(List<Opportunity> opportunityResponse) {

		StringBuilder content = new StringBuilder();

		content.append("\n");

		opportunityResponse.stream().forEach(opportunity -> {

			String crmOwner, accountId = null, name = null, globalAccount = null, sapLogoNumber = null;

			if (opportunity.getCrmOwner() == null)
				crmOwner = null;
			else
				crmOwner = opportunity.getCrmOwner().getName();

			if (opportunity.getAccount() != null) {
				accountId = opportunity.getAccount().getAccoundId();
				name = opportunity.getAccount().getName();
				globalAccount = opportunity.getAccount().getGlobalAccount();
				sapLogoNumber = opportunity.getAccount().getSapLogoNumber();
			}

			content.append(accountId + "	" + name + "	" + opportunity.getOpportunityId() + "	"
					+ opportunity.getName() + "	" + sapLogoNumber + "	" + globalAccount + "	"
					+ opportunity.getRecordType().getName() + "	" + opportunity.getBusinessType() + "	"
					+ opportunity.getOwner().getName() + "	" + opportunity.getOwner().getUserId() + "	"
					+ opportunity.getOwner().getSalesTeam() + "	" + opportunity.getOwner().getSalesGeo() + "	"
					+ opportunity.getOwner().getSalesRegion() + "	" + opportunity.getOwner().getSalesTerritory()
					+ "	" + opportunity.getOwner().getSalesRole() + "	" + opportunity.getCloseDate() + "	"
					+ opportunity.getStageName() + "	" + opportunity.getNewYearGrowthBooking() + "	"
					+ opportunity.getServicesBoking() + "	" + opportunity.getBeyondNewYearGrowthBooking() + "	"
					+ opportunity.getBeyondServicesBooking() + "	" + opportunity.getRenewableBookingAtTerm() + "	"
					+ opportunity.getRenewableBookingBeyondTerm() + "	" + opportunity.getExpiringContractTCV()
					+ "	" + opportunity.getExpiringContractDuration() + "	" + crmOwner + "	"
					+ opportunity.getSapContractNumber() + "	" + opportunity.getSharePointDealId() + "	"
					+ dateFormat.format(new Date()));

			content.append("\n");
		});

		return content.toString();
	}
}
