package com.synopsys.anaplanintegration.service;

import com.synopsys.anaplanintegration.entity.QueryRequest;

public interface SfdcOperationsService {
	
	String  getQueryResult(QueryRequest  queryRequest); 

}
