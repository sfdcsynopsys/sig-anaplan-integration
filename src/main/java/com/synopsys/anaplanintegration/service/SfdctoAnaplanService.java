package com.synopsys.anaplanintegration.service;

import java.util.List;

import com.synopsys.anaplanintegration.entity.SfdcProperties;
import com.synopsys.anaplanintegration.entity.SfdcSyncResponse;

public interface SfdctoAnaplanService {

	SfdcSyncResponse exportFromSfdctoAnaplan(List<SfdcProperties> sfdcProperties);
	
}
