package com.synopsys.anaplanintegration.service.impl;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synopsys.anaplanintegration.entity.OpportunityQueryResponse;
import com.synopsys.anaplanintegration.entity.QueryRequest;
import com.synopsys.anaplanintegration.entity.SalesQueryResponse;
import com.synopsys.anaplanintegration.entity.SfdcProperties;
import com.synopsys.anaplanintegration.entity.SfdcSyncResponse;
import com.synopsys.anaplanintegration.exception.CustomException;
import com.synopsys.anaplanintegration.service.AnaplanOperationsService;
import com.synopsys.anaplanintegration.service.SfdcOperationsService;
import com.synopsys.anaplanintegration.service.SfdctoAnaplanService;
import com.synopsys.anaplanintegration.util.CustomJsonToFile;

@Service
public class SfdctoAnaplanServiceImpl implements SfdctoAnaplanService {

	private AnaplanOperationsService anaplanOperationService;

	private SfdcOperationsService sfdcOperationService;

	ReentrantLock lock = new ReentrantLock();

	private CustomJsonToFile customJsonTofile = new CustomJsonToFile();

	private static final Logger logger = LogManager.getLogger(SfdctoAnaplanServiceImpl.class);

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	public SfdctoAnaplanServiceImpl(AnaplanOperationsService anaplanOperationService,
			SfdcOperationsService sfdcOperationService) {

		this.anaplanOperationService = anaplanOperationService;
		this.sfdcOperationService = sfdcOperationService;

	}

	@Autowired
	private JmsTemplate jmsTemplate;

	@Override
	public SfdcSyncResponse exportFromSfdctoAnaplan(List<SfdcProperties> sfdcProperties) {

		String soqlQueryResponse;
		String nextRecordUrl = "/services/data/v42.0/query";
		int totalCount = 0,i = 0;

		QueryRequest queryRequest = new QueryRequest();

		SalesQueryResponse salesQueryResponse;
		OpportunityQueryResponse opportunityQueryResponse;
		StringBuilder salesFileResponse = new StringBuilder();
		StringBuilder oppFileResponse = new StringBuilder();
		SfdcSyncResponse sfdcSyncResponse = new SfdcSyncResponse();

		salesFileResponse
				.append("Opportunity ID (18)	Plane Sourced	Sales Qualified Territory	Opportunity Record Type	SDR Sourced	Sales Qualified Date	Sales Qualified Status	Business Type	Account Name	"
						+ "Opportunity Name	Stage	Amount	Last Updated Date");

		oppFileResponse
				.append("Account ID (18)	Account Name	Opportunity ID (18)	Opportunity Name	SAP Site Logo	Global Account	Opportunity Record Type	Business Type	Opportunity Owner	"
						+ "USER ID (18)	Sales Team	Sales Geo	Sales Region	Sales Territory	Sales Role	Close Date	Stage	"
						+ "1st Year New/Growth Booking	1st  Services Booking	Beyond 1st Year New/Growth Booking 	"
						+ "Beyond 1st Services Booking	Renewable Booking at Term	"
						+ "Renewable Booking Beyond Term	Expiring Contract TCV	Expiring Contract Duration	CRM Owner	SAP Contract Number	SharePoint deal ID	Last Updated Date");

		if (!lock.isLocked()) {

			lock.lock();
			try {

				for (i = 0; i < sfdcProperties.size(); i++) {
	
					if (sfdcProperties.get(i).getFileName().equals("Opportunities")) {

						do {

							queryRequest.setNextrecordUrl(nextRecordUrl);

							queryRequest.setSoqlQuery(sfdcProperties.get(i).getSoqlQuery());

							soqlQueryResponse = sfdcOperationService.getQueryResult(queryRequest);

							opportunityQueryResponse = mapper.readValue(soqlQueryResponse,
									OpportunityQueryResponse.class);

							logger.info(opportunityQueryResponse.getRecords().size() + " Records Fetched Successfully");

							totalCount = opportunityQueryResponse.getRecords().size() + totalCount;

							oppFileResponse.append(
									customJsonTofile.convertOpportunityToFile(opportunityQueryResponse.getRecords()));

							nextRecordUrl = opportunityQueryResponse.getNextRecordsUrl();

						} while (opportunityQueryResponse.getNextRecordsUrl() != null);

						logger.info("File Uploading for Opportunities");

						anaplanOperationService.uploadFile(sfdcProperties.get(i).getFileId(),
								oppFileResponse.toString());

						logger.info("Opportunities File Upload Successffully");

					} else if (sfdcProperties.get(i).getFileName().equals("Sales_Qualifying_Lead")) {

						nextRecordUrl = "/services/data/v42.0/query";

						do {

							queryRequest.setNextrecordUrl(nextRecordUrl);

							queryRequest.setSoqlQuery(sfdcProperties.get(i).getSoqlQuery());

							soqlQueryResponse = sfdcOperationService.getQueryResult(queryRequest);

							salesQueryResponse = mapper.readValue(soqlQueryResponse, SalesQueryResponse.class);

							logger.info(salesQueryResponse.getRecords().size() + " Records Fetched Successfully");

							totalCount = salesQueryResponse.getRecords().size() + totalCount;

							salesFileResponse
									.append(customJsonTofile.convertSalesToFile(salesQueryResponse.getRecords()));

							nextRecordUrl = salesQueryResponse.getNextRecordsUrl();

						} while (salesQueryResponse.getNextRecordsUrl() != null);

						logger.info("File Uploading for Sales Qulaifying Lead");

						anaplanOperationService.uploadFile(sfdcProperties.get(i).getFileId(),
								salesFileResponse.toString());

						logger.info("Sales Qualifying Lead File Upload Successffully");

					}

					else {
						logger.info("Wrong File Name");
					}

					anaplanOperationService.runProcess(sfdcProperties.get(i).getProcessId());

					logger.info("Process Run Successffully: " + sfdcProperties.get(i).getProcessId());

					anaplanOperationService.runMasterProcess(sfdcProperties.get(i).getMasterProcessId());

					logger.info("Data Has uploaded in to Master Model Successffully: "
							+ sfdcProperties.get(i).getMasterProcessId());

					anaplanOperationService.runMasterProcess(sfdcProperties.get(i).getExplosionProcessId());

					logger.info("Data Has exploded in to Modules Successffully: "
							+ sfdcProperties.get(i).getExplosionProcessId());

					sfdcSyncResponse.setTrigger(sfdcProperties.get(i).getJobType());
					sfdcSyncResponse.setFileName(sfdcProperties.get(i).getFileName());
					sfdcSyncResponse.setQuery(sfdcProperties.get(i).getSoqlQuery());
					sfdcSyncResponse.setCount(totalCount);
					sfdcSyncResponse.setResult("Success");

					jmsTemplate.convertAndSend("AnaplanLogQueue", sfdcSyncResponse);

				}

			} catch (Exception ex) {

				
				sfdcSyncResponse.setTrigger(sfdcProperties.get(i).getJobType());
				sfdcSyncResponse.setFileName(sfdcProperties.get(i).getFileName());
				sfdcSyncResponse.setQuery(sfdcProperties.get(i).getSoqlQuery());
				sfdcSyncResponse.setCount(totalCount);
				sfdcSyncResponse.setResult("Failure");
				sfdcSyncResponse.setMessage(ex.getMessage());

				jmsTemplate.convertAndSend("AnaplanLogQueue", sfdcSyncResponse);
				
				throw new CustomException(ex.getMessage());

			}

			finally {
				lock.unlock();
				logger.info("Resource is Unlocked");
			}
		} else {
			logger.info("A lock has been initiated");
		}

		return null;
	}

}
