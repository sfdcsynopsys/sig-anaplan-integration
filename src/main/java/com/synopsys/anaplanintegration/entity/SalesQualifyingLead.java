package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesQualifyingLead {
	
	
	private Attributes attributes;
	
	@JsonProperty("Amount")
	private String amount;
	
	@JsonProperty("Business_Type__c")
	private String business_Type_c;
	
	@JsonProperty("Name")
	private String Name;
	
	@JsonProperty("Opportunity_ID_18__c")
	private String opportunityId;
	
	@JsonProperty("Plane_Sourced__c")
	private String planeSourced;
	
	@JsonProperty("Sales_Qualified_Date__c")
	private String salesQualifiedDate;
	
	@JsonProperty("Sales_Status__c")
	private String salesStatus;
	
	@JsonProperty("Sales_Qualified_Territory__c")
	private String salesQualifiedTerritory;
	
	@JsonProperty("SDR_Sourced__r")
	private SdrSourced sdrSourced;
	
	@JsonProperty("StageName")
	private String stageName;
	
	@JsonProperty("Account")
	private Account salesAccount;
	
	@JsonProperty("RecordType")
	private RecordType recordType;
	


}
