package com.synopsys.anaplanintegration.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QueryRequest {
	
	private String soqlQuery;
	
	private String nextrecordUrl;
	
}
