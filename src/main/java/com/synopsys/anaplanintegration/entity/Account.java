package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {
	

	private Attributes attributes;
	
	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("Account_ID_18__c")
	private String accoundId;
    
	@JsonProperty("Global_Account__c")
	private String globalAccount;
	
	@JsonProperty("SAP_Logo_Number__c")
	private String sapLogoNumber;
	
	
	

}
