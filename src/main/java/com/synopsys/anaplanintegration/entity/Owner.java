package com.synopsys.anaplanintegration.entity;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Owner {

	@Nullable
	@JsonProperty("attributes")
	private Attributes attributes;
	
	@Nullable
	@JsonProperty("Name")
	private String name;
	
	@Nullable
	@JsonProperty("Sales_Geo__c")
	private String salesGeo;
	
	@Nullable
	@JsonProperty("Sales_Region__c")
	private String salesRegion;
	
	@Nullable
	@JsonProperty("Sales_Role__c")
	private String salesRole;
	
	@Nullable
	@JsonProperty("Sales_Team__c")
	private String salesTeam;
	
	@Nullable
	@JsonProperty("Sales_Territory__c")
	private String salesTerritory;
	
	@Nullable
	@JsonProperty("USER_ID_18__c")
	private String userId;
	
	
	
}

