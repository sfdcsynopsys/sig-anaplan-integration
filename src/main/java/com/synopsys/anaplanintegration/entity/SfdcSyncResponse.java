package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SfdcSyncResponse {
	
	
	private String logId;
	
	private String timeStamp;
	
	private String trigger;
	
	private String fileName;
	
	private String query;
	
	private String result;
	
	private int count;
	
	private String message;


	
}
