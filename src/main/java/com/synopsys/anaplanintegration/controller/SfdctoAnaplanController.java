package com.synopsys.anaplanintegration.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.synopsys.anaplanintegration.entity.SfdcProperties;
import com.synopsys.anaplanintegration.entity.SfdcSyncResponse;
import com.synopsys.anaplanintegration.service.SfdctoAnaplanService;

@RestController
public class SfdctoAnaplanController {

	private static final Logger logger = LogManager.getLogger(SfdctoAnaplanController.class);

	private SfdctoAnaplanService sfdctoAnaplanService;

	@Autowired
	public SfdctoAnaplanController(SfdctoAnaplanService sfdctoAnaplanService) {

		this.sfdctoAnaplanService = sfdctoAnaplanService;
	}

	@PostMapping("/sync/sfdc-anaplan")
	public ResponseEntity<SfdcSyncResponse> exportFromSfdctoAnaplan(@Valid @RequestBody List<SfdcProperties> sfdcProperties) throws IOException {

		SfdcSyncResponse sfdcSyncResponse;
		
		sfdcSyncResponse=sfdctoAnaplanService.exportFromSfdctoAnaplan(sfdcProperties);

		return new ResponseEntity<>(sfdcSyncResponse, HttpStatus.CREATED);

	}

}
