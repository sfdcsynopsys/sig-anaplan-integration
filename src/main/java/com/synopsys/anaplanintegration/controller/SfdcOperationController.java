package com.synopsys.anaplanintegration.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.synopsys.anaplanintegration.entity.QueryRequest;
import com.synopsys.anaplanintegration.service.SfdcOperationsService;

@RestController
public class SfdcOperationController {

	private static final Logger LOGGER = LogManager.getLogger(SfdcOperationController.class);

	private SfdcOperationsService sfdcOperaionsService;
	
	@Autowired
	public SfdcOperationController(SfdcOperationsService sfdcOperaionsService) {
		this.sfdcOperaionsService = sfdcOperaionsService;
	}
	
	
	@GetMapping("/query")
	public String getQueryResult(@Valid @RequestBody QueryRequest  queryRequest){
		
		return sfdcOperaionsService.getQueryResult(queryRequest);
	}
	
	
	
	
}
